package com.thenextlogics.myapplication.databse;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.thenextlogics.myapplication.common.CustomeControlls.Constant;
import com.thenextlogics.myapplication.controlls.AppControler;
import com.thenextlogics.myapplication.model.AsmaUlHusnaModel;
import com.thenextlogics.myapplication.model.AsmaUlNabiModel;
import com.thenextlogics.myapplication.model.DuaModel;
import com.thenextlogics.myapplication.model.HadithModel;
import com.thenextlogics.myapplication.model.QuranModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Salman Saleem on 1/19/2017.
 */

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {

    @SuppressLint("SdCardPath")
    private static final String DATABASE_NAME = Environment
            .getExternalStorageDirectory().getAbsolutePath() + "/Android/data/com.thenextlogics.myapplication/auqatesalat.db";
    private static final String DATABASE_PATH = Environment
            .getExternalStorageDirectory().getAbsolutePath() + "/Android/data/com.thenextlogics.myapplication/";
    public static final int DATABASE_VERSION = 1;

    private Dao<AsmaUlNabiModel, Integer> asmaUlNabiModelIntegerDao = null;
    private Dao<AsmaUlHusnaModel,Integer>asmaUlHusnaModelIntegerDao = null;
    private Dao<DuaModel,Integer>duaModelIntegerDao = null;
    private Dao<HadithModel,Integer>hadithModelIntegerDao = null;
    private Dao<QuranModel,Integer>quranModelIntegerDao = null;
    private static final AtomicInteger usageCounter = new AtomicInteger(0);
    private static DataBaseHelper dataBaseHelper;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        boolean dbexist = checkdatabase();
        if (!dbexist) {

            //      If database did not exist, try copying existing database from assets folder.
            try {
                File dir = new File(DATABASE_PATH);
                dir.mkdirs();
                InputStream myinput = context.getAssets().open("auqatesalat.db");
                String outfilename = DATABASE_PATH + "auqatesalat.db";
                // Log.i(DatabaseHelper.class.getName(), "DB Path : " + outfilename);
                OutputStream myoutput = new FileOutputStream(outfilename);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = myinput.read(buffer)) > 0) {
                    myoutput.write(buffer, 0, length);
                }
                myoutput.flush();
                myoutput.close();
                myinput.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkdatabase() {
        boolean checkdb = false;
        String myPath = DATABASE_PATH + "auqatesalat.db";
        File dbfile = new File(myPath);
        checkdb = dbfile.exists();
        Log.i("DBExists", "DB Exist : " + checkdb);
        return checkdb;
    }

    public static synchronized DataBaseHelper getSingletonInstance(Context context) {
        if (dataBaseHelper == null) {

            dataBaseHelper = new DataBaseHelper(context);
        }
        usageCounter.incrementAndGet();
        return dataBaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
        try {
            Log.i(DataBaseHelper.class.getName(), "onUpgrade");

        } catch (Exception e) {
            Log.e(DataBaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public Dao<AsmaUlNabiModel, Integer> getAsmaUlNabiModelIntegerDao() throws SQLException {
        if (asmaUlNabiModelIntegerDao == null) {
            asmaUlNabiModelIntegerDao = getDao(AsmaUlNabiModel.class);
        }
        return asmaUlNabiModelIntegerDao;
    }
    public Dao<AsmaUlHusnaModel, Integer> getAsmaUlHusnaModelIntegerDao() throws SQLException {
        if (asmaUlHusnaModelIntegerDao == null) {
            asmaUlHusnaModelIntegerDao = getDao(AsmaUlHusnaModel.class);
        }
        return asmaUlHusnaModelIntegerDao;
    }
    public Dao<DuaModel,Integer>getDuaModelIntegerDao()throws SQLException{
        if (duaModelIntegerDao==null){
            duaModelIntegerDao= getDao(DuaModel.class);
        }
        return duaModelIntegerDao;
    }
   public Dao<HadithModel, Integer>getHadithModelIntegerDao()throws  SQLException{
       if (hadithModelIntegerDao== null){
           hadithModelIntegerDao = getDao(HadithModel.class);
       }
       return hadithModelIntegerDao;
   }
    public Dao<QuranModel, Integer>getQuranModelIntegerDao()throws  SQLException{
        if (quranModelIntegerDao== null){
            quranModelIntegerDao = getDao(QuranModel.class);
        }
        return quranModelIntegerDao;
    }
    @Override
    public void close() {
        if (usageCounter.decrementAndGet() == 0) {
            super.close();

            quranModelIntegerDao = null;
            asmaUlNabiModelIntegerDao = null;
            asmaUlHusnaModelIntegerDao = null;
            duaModelIntegerDao = null;
            hadithModelIntegerDao = null;
            dataBaseHelper = null;
        }
    }
}
