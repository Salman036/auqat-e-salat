package com.thenextlogics.myapplication.adaptar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.Constant;
import com.thenextlogics.myapplication.common.CustomeControlls.OnItemClick;
import com.thenextlogics.myapplication.model.AsmaUlNabiModel;

import java.util.List;
import java.util.Random;

/**
 * Created by Salman Saleem on 1/18/2017.
 */

public class AsmaUlNabiAdaptar extends RecyclerView.Adapter<AsmaUlNabiAdaptar.ViewHolders> {

    List<AsmaUlNabiModel>asmaUlNabiModelList;
    int resource;
    private int lastPosition = -1;
    Context context;
    OnItemClick onItemClick;
    public class ViewHolders extends RecyclerView.ViewHolder {
       public RelativeLayout rl_asma_ul_nabi;
      public   TextView tv_asma_ulnabi_title,tv_asma_ulbnabi_urdu,tv_asma_uslnabi_mening;
        public ViewHolders(View itemView) {
            super(itemView);
            tv_asma_ulbnabi_urdu = (TextView)itemView.findViewById(R.id.tv_asma_ulbnabi_urdu);

            tv_asma_ulnabi_title = (TextView)itemView.findViewById(R.id.tv_asma_ulnabi_title);
            tv_asma_uslnabi_mening = (TextView)itemView.findViewById(R.id.tv_asma_uslnabi_mening);
            rl_asma_ul_nabi = (RelativeLayout)itemView.findViewById(R.id.rl_asma_ul_nabi_row);
        }
    }
    public AsmaUlNabiAdaptar(List<AsmaUlNabiModel>asmaUlNabiModels, OnItemClick onItemClick){
        this.asmaUlNabiModelList = asmaUlNabiModels;
        this.onItemClick = onItemClick;
        this.context = context;

    }
    @Override
    public ViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.asmaulnabi_row,parent,false);
        ViewHolders viewh = new ViewHolders(view);
        return viewh;
    }

    @Override
    public void onBindViewHolder(ViewHolders holder, final int position) {
        setScaleAnimation(holder.itemView);
        holder.tv_asma_ulnabi_title.setText(asmaUlNabiModelList.get(position).getEnglish());
        holder.tv_asma_ulbnabi_urdu.setText(asmaUlNabiModelList.get(position).getArabic());
        holder.tv_asma_uslnabi_mening.setText(asmaUlNabiModelList.get(position).getMeaning());
        holder.rl_asma_ul_nabi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(position);
            }
        });
    }
    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(Constant.FADE_DURATION);
        view.startAnimation(anim);
    }
    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(Constant.FADE_DURATION);
        view.startAnimation(anim);
    }
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }
    @Override
    public int getItemCount() {
        return asmaUlNabiModelList.size();
    }


}
