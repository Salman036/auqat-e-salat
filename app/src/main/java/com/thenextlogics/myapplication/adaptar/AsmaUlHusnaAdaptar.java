package com.thenextlogics.myapplication.adaptar;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.Constant;
import com.thenextlogics.myapplication.common.CustomeControlls.OnItemClick;
import com.thenextlogics.myapplication.model.AsmaUlHusnaModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Salman Saleem on 1/18/2017.
 */

public class AsmaUlHusnaAdaptar extends RecyclerView.Adapter<AsmaUlHusnaAdaptar.HusnaHolder> {

    List<AsmaUlHusnaModel> asmaUlHusnaModelList;
    OnItemClick onItemClick;
    public class HusnaHolder extends RecyclerView.ViewHolder{
        TextView tv_husna_title,tv_husna_urdu,tv_husna_meaning;
        RelativeLayout rl_husna_row;
        public HusnaHolder(View itemView) {
            super(itemView);
            tv_husna_title = (TextView)itemView.findViewById(R.id.tv_husna_title);
            tv_husna_urdu = (TextView)itemView.findViewById(R.id.tv_husna_urdu);
            tv_husna_meaning = (TextView)itemView.findViewById(R.id.tv_husna_meaning);
            rl_husna_row = (RelativeLayout) itemView.findViewById(R.id.rl_husna_row);
        }
    }
    @Override
    public HusnaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.asma_ul_husna_row,parent,false);
        HusnaHolder husnaHolder = new HusnaHolder(view);
        return husnaHolder;
    }
    public AsmaUlHusnaAdaptar(List<AsmaUlHusnaModel>asmaUlHusnaModels,OnItemClick onItemClick){
        this.asmaUlHusnaModelList = asmaUlHusnaModels;
        this.onItemClick = onItemClick;

    }
    @Override
    public void onBindViewHolder(HusnaHolder holder, final int position) {
        setScaleAnimation(holder.itemView);
        holder.tv_husna_title.setText(asmaUlHusnaModelList.get(position).getEnglish());
        holder.tv_husna_urdu.setText(asmaUlHusnaModelList.get(position).getArabic());
        holder.tv_husna_meaning.setText(asmaUlHusnaModelList.get(position).getTrans());
        holder.rl_husna_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(position);
            }
        });

    }
    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(Constant.FADE_DURATION);
        view.startAnimation(anim);
    }
    @Override
    public int getItemCount() {
        return asmaUlHusnaModelList.size();
    }


}
