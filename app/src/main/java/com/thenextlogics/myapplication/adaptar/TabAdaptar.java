package com.thenextlogics.myapplication.adaptar;

import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.thenextlogics.myapplication.fragments.CalenderFragment;
import com.thenextlogics.myapplication.fragments.MenuFragment;
import com.thenextlogics.myapplication.fragments.PrayHomeFragment;
import com.thenextlogics.myapplication.fragments.PrayTimeFragment;
import com.thenextlogics.myapplication.fragments.QiblaFragment;

/**
 * Created by Salman Saleem on 1/22/2017.
 */

public class TabAdaptar extends FragmentStatePagerAdapter {


    public TabAdaptar(FragmentManager fragmentManager ) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new MenuFragment();
            case 1:
                return new PrayHomeFragment();

            default:
                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
