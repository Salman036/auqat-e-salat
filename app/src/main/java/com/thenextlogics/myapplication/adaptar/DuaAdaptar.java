package com.thenextlogics.myapplication.adaptar;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.Constant;
import com.thenextlogics.myapplication.common.CustomeControlls.OnItemClick;
import com.thenextlogics.myapplication.model.DuaModel;

import java.util.List;

/**
 * Created by Salman Saleem on 1/20/2017.
 */

public class DuaAdaptar extends RecyclerView.Adapter<DuaAdaptar.DuaHolder> {

    List<DuaModel>duaModelList;
    OnItemClick onItemClick;
    public class DuaHolder extends RecyclerView.ViewHolder{
        TextView tv_dua_ref,tv_dua_arabic,tv_dua_english;
        RelativeLayout rl_dua;
        public DuaHolder(View itemView) {
            super(itemView);
            tv_dua_ref = (TextView)itemView.findViewById(R.id.tv_dua_ref);
            tv_dua_arabic = (TextView)itemView.findViewById(R.id.tv_dua_arabic);
            tv_dua_english = (TextView)itemView.findViewById(R.id.tv_dua_english);
            rl_dua = (RelativeLayout) itemView.findViewById(R.id.rl_dua);
        }
    }
    public DuaAdaptar(List<DuaModel>duaModelList,OnItemClick onItemClick){
        this.onItemClick = onItemClick;
        this.duaModelList = duaModelList;
    }
    @Override
    public DuaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dua_row,parent,false);
        DuaHolder holder = new DuaHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DuaHolder holder, final int position) {
        setScaleAnimation(holder.itemView);
        holder.tv_dua_english.setText(duaModelList.get(position).getEnglish());
        holder.tv_dua_arabic.setText(duaModelList.get(position).getArabic());
        holder.tv_dua_ref.setText(duaModelList.get(position).getReference());
        holder.rl_dua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(position);
            }
        });
    }
    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(Constant.FADE_DURATION);
        view.startAnimation(anim);
    }
    @Override
    public int getItemCount() {
        return duaModelList.size();
    }


}
