package com.thenextlogics.myapplication.adaptar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.Constant;
import com.thenextlogics.myapplication.common.CustomeControlls.OnItemClick;
import com.thenextlogics.myapplication.model.HadithModel;

import java.util.List;

/**
 * Created by Salman Saleem on 1/23/2017.
 */

public class HadithAdaptar extends RecyclerView.Adapter<HadithAdaptar.HadithHolder> {
    List<HadithModel>hadithModels;
    OnItemClick onItemClick;
    Context context;
    public class HadithHolder extends RecyclerView.ViewHolder{
        TextView tv_hadees_arabic,tv_hadees_english;
        RelativeLayout rl_hades;
        public HadithHolder(View itemView) {
            super(itemView);
            tv_hadees_arabic = (TextView)itemView.findViewById(R.id.tv_hadees_arabic);
            tv_hadees_english = (TextView)itemView.findViewById(R.id.tv_hadees_english);
            rl_hades = (RelativeLayout) itemView.findViewById(R.id.rl_hades);
        }
    }
    public HadithAdaptar(Context context,List<HadithModel>hadithModels,OnItemClick onItemClick){
        this.onItemClick = onItemClick;
        this.hadithModels = hadithModels;
        this.context = context;
    }
    @Override
    public HadithHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hadees_row,parent,false);
       HadithHolder holder = new HadithHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(HadithHolder holder, final int position) {
        setScaleAnimation(holder.itemView);
        holder.tv_hadees_arabic.setText(hadithModels.get(position).getArabic_hadith());
        holder.tv_hadees_english.setText(hadithModels.get(position).getHadees());
        holder.rl_hades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return hadithModels.size();
    }
    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(Constant.FADE_DURATION);
        view.startAnimation(anim);
    }

}
