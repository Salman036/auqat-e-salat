package com.thenextlogics.myapplication.adaptar;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.model.QuranModel;

import java.util.List;

/**
 * Created by Salman Saleem on 1/28/2017.
 */

public class QuranAdaptar extends RecyclerView.Adapter<QuranAdaptar.QuranHolder> {

    List<QuranModel>quranModels;
    public class QuranHolder  extends RecyclerView.ViewHolder{
        TextView tv_quran;
        public QuranHolder(View itemView) {
            super(itemView);
            tv_quran = (TextView)itemView.findViewById(R.id.tv_detail_quran);
        }
    }
    public QuranAdaptar(List<QuranModel>quranModels){
        this.quranModels = quranModels;
    }
    @Override
    public QuranHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quran_row,parent,false);
        QuranHolder holder = new QuranHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(QuranHolder holder, int position) {
        holder.tv_quran.setText(quranModels.get(position).getIquran());
    }

    @Override
    public int getItemCount() {
        return quranModels.size();
    }
}
