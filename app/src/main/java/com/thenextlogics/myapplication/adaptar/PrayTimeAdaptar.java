package com.thenextlogics.myapplication.adaptar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.Constant;
import com.thenextlogics.myapplication.model.PrayTimeModel;

import java.util.List;

/**
 * Created by Salman Saleem on 1/18/2017.
 */

public class PrayTimeAdaptar extends RecyclerView.Adapter<PrayTimeAdaptar.TimeHolder>{

    List<PrayTimeModel>prayTimeModelsList;
    Context context;
    public class TimeHolder extends RecyclerView.ViewHolder{
        TextView tv_pray_time,tv_pray_name;
        public TimeHolder(View itemView) {
            super(itemView);
            tv_pray_name = (TextView)itemView.findViewById(R.id.tv_pray_name);
            tv_pray_time = (TextView)itemView.findViewById(R.id.tv_pray_time);
        }
    }
    @Override
    public TimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pray_time_row,parent,false);
        TimeHolder timeHolder = new TimeHolder(view);

        return timeHolder;
    }
    public PrayTimeAdaptar(Context context, List<PrayTimeModel>prayTimeModels){
        this.prayTimeModelsList = prayTimeModels;
        this.context = context;
    }
    @Override
    public void onBindViewHolder(TimeHolder holder, int position) {
        setScaleAnimation(holder.itemView);
        holder.tv_pray_time.setText(prayTimeModelsList.get(position).getPrayTime());
        holder.tv_pray_name.setText(prayTimeModelsList.get(position).getPrayName());
    }
    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(Constant.FADE_DURATION);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return prayTimeModelsList.size();
    }


}
