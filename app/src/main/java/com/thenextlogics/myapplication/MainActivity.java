package com.thenextlogics.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.thenextlogics.myapplication.common.CustomeControlls.FragmentTrans;
import com.thenextlogics.myapplication.fragments.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentTrans.addFragment(new MainFragment(),this);
    }
}
