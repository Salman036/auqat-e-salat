package com.thenextlogics.myapplication.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.adaptar.DuaAdaptar;
import com.thenextlogics.myapplication.adaptar.HadithAdaptar;
import com.thenextlogics.myapplication.common.CustomeControlls.FragmentTrans;
import com.thenextlogics.myapplication.common.CustomeControlls.OnItemClick;
import com.thenextlogics.myapplication.controlls.DataBaseController;
import com.thenextlogics.myapplication.model.HadithModel;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HadeesFragment extends Fragment implements View.OnClickListener {


    public HadeesFragment() {
        // Required empty public constructor
    }
    List<HadithModel>hadithModels;
    ArrayList<HadithModel>hadithModelArrayList;
    RecyclerView haadees_recycler;
    TextView tv_toolbar_title;
    LinearLayoutManager linearLayoutManager;
    RelativeLayout rl_back;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_hadees, container, false);
        initView(rootView);
        return rootView;
    }
    public void initView(View view){
        haadees_recycler = (RecyclerView)view.findViewById(R.id.haadees_recycler);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        haadees_recycler.setLayoutManager(linearLayoutManager);
        tv_toolbar_title = (TextView)view.findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText("Hadith");
        hadithModelArrayList = new ArrayList<>();

        rl_back = (RelativeLayout)view.findViewById(R.id.rl_back);
        rl_back.setOnClickListener(this);
        try {
            hadithModels= DataBaseController.getSingoltonInstance().getHadithTable(getActivity());
            for (int i = 0; i<hadithModels.size();i++){
                hadithModelArrayList.add(hadithModels.get(i));
            }
            HadithAdaptar adaptar = new HadithAdaptar(getActivity(), hadithModels, new OnItemClick() {
                @Override
                public void onClick(int position) {
                    FragmentTrans.addFragmentwithRecycler(new HadithDetailFragment(),getActivity(),"id",position,"hadithList",
                            hadithModelArrayList,HadithDetailFragment.class.getName());
                }
            });
            haadees_recycler.setAdapter(adaptar);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_back:
                getFragmentManager().popBackStack();
                break;
            default:
                break;
        }
    }
}
