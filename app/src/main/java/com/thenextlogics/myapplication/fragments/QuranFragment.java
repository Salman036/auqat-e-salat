package com.thenextlogics.myapplication.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.adaptar.QuranAdaptar;
import com.thenextlogics.myapplication.controlls.DataBaseController;
import com.thenextlogics.myapplication.model.QuranModel;

import java.sql.SQLException;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuranFragment extends Fragment implements View.OnClickListener {


    public QuranFragment() {
        // Required empty public constructor
    }

    private RecyclerView quran_recycler;
    private LinearLayoutManager linearLayoutManager;
    private List<QuranModel> quranModels;
    private TextView tv_toolbar_title;
    RelativeLayout rl_bacl;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_quran, container, false);
        initViiew(rootView);
        return rootView;
    }

    private void initViiew(View view) {
        quran_recycler = (RecyclerView) view.findViewById(R.id.quran_recycler);
        tv_toolbar_title = (TextView)view.findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getText(R.string.quran));
        rl_bacl = (RelativeLayout)view.findViewById(R.id.rl_back);
        rl_bacl.setOnClickListener(this);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        quran_recycler.setLayoutManager(linearLayoutManager);
        try {
            quranModels = DataBaseController.getSingoltonInstance().getQuran(getActivity());
            QuranAdaptar adaptar = new QuranAdaptar(quranModels);
            quran_recycler.setAdapter(adaptar);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void setValue() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_back:
                getFragmentManager().popBackStack();
                break;
            default:
                break;
        }
    }
}
