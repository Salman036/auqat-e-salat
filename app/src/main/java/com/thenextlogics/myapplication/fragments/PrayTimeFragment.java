package com.thenextlogics.myapplication.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;
import java.util.Date;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.adaptar.PrayTimeAdaptar;
import com.thenextlogics.myapplication.common.CustomeControlls.GPSTracker;
import com.thenextlogics.myapplication.common.CustomeControlls.PrayTimeSchedule;
import com.thenextlogics.myapplication.common.CustomeControlls.Utils;
import com.thenextlogics.myapplication.model.PrayTimeModel;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrayTimeFragment extends Fragment implements View.OnClickListener {


    public PrayTimeFragment() {
        // Required empty public constructor
    }
    TextView tv_toolbar_title;
    RelativeLayout rl_back;
    LinearLayoutManager linearLayoutManager;
    RecyclerView pray_recycler;
    List<PrayTimeModel>prayTimeModels;
    double latitude = 0;
    double longitude  = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_pray_time, container, false);
        initViews(rootView);
        return rootView;
    }
    private void initViews(View view){

        tv_toolbar_title = (TextView)view.findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getText(R.string.prayer));
        tv_toolbar_title.setTextColor(Color.BLACK);
        rl_back = (RelativeLayout)view.findViewById(R.id.rl_back);
        rl_back.setOnClickListener(this);
        prayTimeModels=new ArrayList<>();
        pray_recycler = (RecyclerView)view.findViewById(R.id.pray_recycler);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        pray_recycler.setLayoutManager(linearLayoutManager);
        if (Utils.latitude!=0.0 && Utils.longitude!=0.0){
            double lat=    Utils.latitude; // returns latitude
            double longi=    Utils.longitude;
            getTime(lat, longi);
        }
        else {
            GPSTracker gps = new GPSTracker(getActivity());
            if (gps.canGetLocation()) { // gps enabled} // return boolean true/false
                double lat=    gps.getLatitude(); // returns latitude
                double longi=    gps.getLongitude(); // returns longitude
                // Toast.makeText(this, "Your Location is" + "Latitude"+ String.valueOf(lat) + "Longitude"+ String.valueOf(longi), Toast.LENGTH_LONG).show();

                getTime(lat, longi);
            }
            else {
                gps.showSettingsAlert(getActivity());
            }
        }
    }
    public void getTime(double _latitude,double _longitude){

        List<String> prayersNames=new ArrayList<>();
        prayersNames.add("Fajar");
        prayersNames.add("Sharooq");
        prayersNames.add("Dhuhar");
        prayersNames.add("Asar");
        prayersNames.add("Sunset");
        prayersNames.add("Magrib");
        prayersNames.add("Isha");
        double timezone = (Calendar.getInstance().getTimeZone()
                .getOffset(Calendar.getInstance().getTimeInMillis()))
                / (1000 * 60 * 60);
        PrayTimeSchedule prayerTime2=new PrayTimeSchedule();
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        // PrayerTime2.getPrayerTimes(cal, _latitude, _longitude, timezone);
        LinkedHashMap<String, String> list2=    getPrayerTimes(_latitude, _longitude);


        List<String> list = new ArrayList<String>(list2.values());


        for (int i=0; i<prayersNames.size(); i++)
        {
            PrayTimeModel prayersModel=new PrayTimeModel();
            prayersModel.setPrayName(prayersNames.get(i).toString());
            prayersModel.setPrayTime(list.get(i).toString());
            prayTimeModels.add(prayersModel);
        }

        PrayTimeAdaptar prayerAdapter=new PrayTimeAdaptar(getActivity(), prayTimeModels);

        pray_recycler.setAdapter(prayerAdapter);

    }


    public  LinkedHashMap<String, String> getPrayerTimes( double lat, double lng) {

        double latitude = lat;
        double longitude = lng;

        //Get time zone instance
        TimeZone defaultTz = TimeZone.getDefault();

        //Get calendar object with current date/time
        Calendar defaultCalc = Calendar.getInstance(defaultTz);

        //Get offset from UTC, accounting for DST
        int defaultTzOffsetMs = defaultCalc.get(Calendar.ZONE_OFFSET) + defaultCalc.get(Calendar.DST_OFFSET);
        double timezone = defaultTzOffsetMs / (1000 * 60 * 60);
        // Test Prayer times here
        PrayTimeSchedule prayers = new PrayTimeSchedule();



        prayers.setTimeFormat(prayers.TIME_12);
        prayers.setCalcMethod(prayers.KARACHI);
        prayers.setAsrJuristic(prayers.HANAFI);
        prayers.setAdjustHighLats(prayers.ANGLE_BASED);

        int[] offsets = {0, 0, 0, 0, 0, 0, 0}; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
        prayers.tune(offsets);

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);

        ArrayList<String> prayerTimes = prayers.getPrayerTimes(cal,
                latitude, longitude, timezone);
        ArrayList<String> prayerNames = prayers.getTimeNames();
        LinkedHashMap<String, String> result = new LinkedHashMap<>();

        for (int i = 0; i < prayerTimes.size(); i++) {
            System.out.println(prayerNames.get(i) + " - " + prayerTimes.get(i));
            result.put(prayerNames.get(i), prayerTimes.get(i));
        }

        return result;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_back:
                getActivity().onBackPressed();
                break;
            default:
                break;
        }
    }
}
