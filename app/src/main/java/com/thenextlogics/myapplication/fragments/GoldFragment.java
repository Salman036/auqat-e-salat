package com.thenextlogics.myapplication.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GoldFragment extends Fragment implements View.OnClickListener {


    public GoldFragment() {
        // Required empty public constructor
    }
    EditText et_gold_24,et_gold_price_24,et_total_gold_24,et_gold_zakat_24,
             et_gold_22,et_gold_price_22,et_total_gold_22,et_gold_zakat_22;
    Button btn_gold_cal;
    TextView tv_zakat_gold_result;

    double num24gold,num24price,num22gold,num22price;
    double result = 0,result_22 = 0,total_result = 0;
    double x = 2.5,y=100;
    double total_amount;
    double zakathGold,zakath22;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_blank, container, false);
        initView(rootView);
        return rootView;
    }
    public void initView(View view){
        et_gold_24 = (EditText)view.findViewById(R.id.et_gold_24);
        et_gold_price_24 = (EditText)view.findViewById(R.id.et_gold_price_24);
        et_total_gold_24 = (EditText)view.findViewById(R.id.et_total_gold_24);
        et_gold_zakat_24 = (EditText)view.findViewById(R.id.et_gold_zakat_24);
        et_gold_price_22 = (EditText)view.findViewById(R.id.et_gold_price_22);
        et_total_gold_22 = (EditText)view.findViewById(R.id.et_total_gold_22);
        et_gold_zakat_22 = (EditText)view.findViewById(R.id.et_gold_zakat_22);
        et_gold_22 = (EditText)view.findViewById(R.id.et_gold_22);
        btn_gold_cal = (Button) view.findViewById(R.id.btn_gold_cal);
        tv_zakat_gold_result = (TextView) view.findViewById(R.id.zakat_gold_result);

        btn_gold_cal.setOnClickListener(this);
    }

    private void calculation(){
        num24gold = Double.parseDouble(et_gold_24.getText().toString());
        num24price = Double.parseDouble(et_gold_price_24.getText().toString());
        num22gold = Double.parseDouble(et_gold_22.getText().toString());
        num22price = Double.parseDouble(et_gold_price_22.getText().toString());


        result = num24gold*num24price;
        et_total_gold_24.setText(String.valueOf(result));
        total_amount = result*x;
        zakathGold = total_amount/y;
        et_gold_zakat_24.setText(String.valueOf(zakathGold));

        result_22 = num22gold * num22price;
        et_total_gold_22.setText(String.valueOf(result_22));
        total_amount = result_22*x;
        zakath22 = total_amount/y;
        et_gold_zakat_22.setText(String.valueOf(zakath22));

        total_result = zakathGold+zakath22;
        tv_zakat_gold_result.setText(String.valueOf(total_result));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_gold_cal:
                calculation();
                break;
            default:
                break;
        }
    }
}
