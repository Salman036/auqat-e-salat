package com.thenextlogics.myapplication.fragments;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.Constant;
import com.thenextlogics.myapplication.common.CustomeControlls.PreferencesManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class DailyAdhkarFragment extends Fragment implements View.OnClickListener {


    public DailyAdhkarFragment() {
        // Required empty public constructor
    }


    Button btnClick, btnReset, btn_save;
    TextView txtCount, toolbar_title;
    TextToSpeech textToSpeech;
    SharedPreferences sharedPreferences;
    RelativeLayout rl_save_back;
    int intCountValue;
    String count;
    public static final String MyPrefrence = "MyPref";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_daily_adhkar, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View view) {
        txtCount = (TextView) view.findViewById(R.id.txtCount);
        toolbar_title = (TextView) view.findViewById(R.id.toolbar_title);
        toolbar_title.setTextColor(Color.BLACK);
        toolbar_title.setText("Tasbih");

        btnClick = (Button) view.findViewById(R.id.btnClick);
        btnReset = (Button) view.findViewById(R.id.btnReset);
        btn_save = (Button) view.findViewById(R.id.btn_save);
        btn_save.setVisibility(View.GONE);
        rl_save_back = (RelativeLayout) view.findViewById(R.id.rl_save_back);

        rl_save_back.setOnClickListener(this);
        btnClick.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        count = PreferencesManager.getPreferences(getActivity(), Constant.ADHKAR_COUNT);
        if (count != null
                && count != ""
                ) {

            txtCount.setText(count);

        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_save_back:
                getFragmentManager().popBackStack();
                break;
            case R.id.btn_save:
                save();
                Toast.makeText(getActivity(),"Save Successfully",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnClick:
                click();
                btn_save.setVisibility(View.VISIBLE);
                break;
            case R.id.btnReset:
                reset();
                btn_save.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }
    private void click(){
        String countValue = txtCount.getText().toString();
        intCountValue = Integer.parseInt(countValue);
        intCountValue++;

        txtCount.setText(String.valueOf(intCountValue));
    }
    private void reset(){
        txtCount.setText(String.valueOf(0));
        PreferencesManager.removeValue(getActivity(),Constant.ADHKAR_COUNT);
    }
    private void save(){
        if (intCountValue>0){
            PreferencesManager.saveValues(getActivity(), Constant.ADHKAR_COUNT,String.valueOf(intCountValue));
//            PreferencesManager.saveValues(getActivity(),Count_Value,String.valueOf(intCountValue));
        }

    }

}
