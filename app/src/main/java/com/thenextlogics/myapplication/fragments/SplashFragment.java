package com.thenextlogics.myapplication.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.FragmentTrans;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashFragment extends Fragment {


    public SplashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_splash, container, false);
        Thread timer = new Thread() {

            @Override
            public void run() {

                try {
                    sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();

                } finally {


                    FragmentTrans.addFragmentWithStack(new MainFragment(),getActivity());

                    // Toast.makeText(Splash.this,"yes",Toast.LENGTH_LONG).show();

                }


            }


        };
        timer.start();
        return rootView;
    }

}
