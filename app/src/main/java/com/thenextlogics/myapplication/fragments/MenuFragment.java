package com.thenextlogics.myapplication.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.FragmentTrans;
import com.thenextlogics.myapplication.common.CustomeControlls.WifiConnectionDetector;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment implements View.OnClickListener {


    public MenuFragment() {
        // Required empty public constructor
    }
    WifiConnectionDetector wifiConnectionDetector;
    Boolean isConnectionExist = false;
    ImageView iv_main_setting,iv_main_share,iv_main_calendar,iv_main_nabi,iv_main_husna,iv__menu_prayers
            ,iv_main_mosque,iv_menu_qibla,iv_main_adhkar,iv_menu_dua,iv_main_hadith,iv_menu_quran,iv_main_zakath;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_menu, container, false);
        initView(rootView);
        return rootView;
    }
    private void connect(){
        isConnectionExist = wifiConnectionDetector.checkmobileConnection();
        if (isConnectionExist) {
            // Internet Connection exists
            showDialog(getActivity(), "Internet Connection",
                    "Your device has WIFI internet access", true);
        } else {
            // Internet connection doesn't exist
            showDialog(getActivity(), "No Internet Connection",
                    "Check your WiFi connection", false);
        }
    }
    private void showDialog(Context context, String title, String message, Boolean status){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setButton(
                "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }
    private void initView(View view){
        wifiConnectionDetector = new WifiConnectionDetector(getActivity());
        iv_main_setting = (ImageView)view.findViewById(R.id.iv_main_setting);
        iv_main_setting.setOnClickListener(this);

        iv_main_share = (ImageView)view.findViewById(R.id.iv_main_share);
        iv_main_share.setOnClickListener(this);

        iv_main_calendar = (ImageView)view.findViewById(R.id.iv_main_calendar);
        iv_main_calendar.setOnClickListener(this);

        iv_main_nabi = (ImageView)view.findViewById(R.id.iv_main_nabi);
        iv_main_nabi.setOnClickListener(this);

        iv_main_husna = (ImageView)view.findViewById(R.id.iv_main_husna);
        iv_main_husna.setOnClickListener(this);

        iv__menu_prayers = (ImageView)view.findViewById(R.id.iv__menu_prayers);
        iv__menu_prayers.setOnClickListener(this);

        iv_main_mosque = (ImageView)view.findViewById(R.id.iv_main_mosque);
        iv_main_mosque.setOnClickListener(this);

        iv_menu_qibla = (ImageView)view.findViewById(R.id.iv_menu_qibla);
        iv_menu_qibla.setOnClickListener(this);

        iv_main_adhkar = (ImageView)view.findViewById(R.id.iv_main_adhkar);
        iv_main_adhkar.setOnClickListener(this);

        iv_menu_dua = (ImageView)view.findViewById(R.id.iv_menu_dua);
        iv_menu_dua.setOnClickListener(this);

        iv_main_hadith = (ImageView)view.findViewById(R.id.iv_main_hadith);
        iv_main_hadith.setOnClickListener(this);

        iv_menu_quran = (ImageView)view.findViewById(R.id.iv_menu_quran);
        iv_menu_quran.setOnClickListener(this);

        iv_main_zakath = (ImageView)view.findViewById(R.id.iv_main_zakath);
        iv_main_zakath.setOnClickListener(this);
    }
    public void shareIt(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
       // sharingIntent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.thenextlogics.myapplication"));
        sharingIntent.setType("image/jpeg");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"");

        startActivity(Intent.createChooser(sharingIntent, "Share via"));
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.thenextlogics.myapplication"));
//        startActivity(intent);
    }
    private void setting(){
        Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_main_setting:
               setting();
                break;
            case R.id.iv_main_share:
                shareIt();
                break;
            case R.id.iv_main_calendar:
                FragmentTrans.addFragmentWithStack(new CalenderFragment(),getActivity());
                break;
            case R.id.iv_main_nabi:
                FragmentTrans.addFragmentWithStack(new AsmaUlNabiFragment(),getActivity());
                break;
            case R.id.iv_main_husna:
                FragmentTrans.addFragmentWithStack(new AsmaUlHusnaFragment(),getActivity());
                break;
            case R.id.iv__menu_prayers:
                FragmentTrans.addFragmentWithStack(new PrayTimeFragment(),getActivity());
                break;
            case R.id.iv_main_mosque:
                if (isConnectionExist==false){
                    connect();
                }
                else {
                    FragmentTrans.addFragmentWithStack(new NearestMasjidFragment(),getActivity());
                }

                break;
            case R.id.iv_menu_qibla:
                FragmentTrans.addFragmentWithStack(new QiblaFragment(),getActivity());
                break;
            case R.id.iv_main_adhkar:
                FragmentTrans.addFragmentWithStack(new DailyAdhkarFragment(),getActivity());
                break;
            case R.id.iv_menu_dua:
                FragmentTrans.addFragmentWithStack(new DuaFragment(),getActivity());
                break;
            case R.id.iv_main_hadith:
                FragmentTrans.addFragmentAnimation(new HadeesFragment(),getActivity());
                break;
            case R.id.iv_menu_quran:
                FragmentTrans.addFragmentWithStack(new QuranFragment(),getActivity());
                break;
            case R.id.iv_main_zakath:
                FragmentTrans.addFragmentWithStack(new ZakatCalculatorFragment(),getActivity());
                break;
            default:
                break;
        }
    }
}
