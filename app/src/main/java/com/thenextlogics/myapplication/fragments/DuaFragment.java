package com.thenextlogics.myapplication.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.adaptar.DuaAdaptar;
import com.thenextlogics.myapplication.common.CustomeControlls.FragmentTrans;
import com.thenextlogics.myapplication.common.CustomeControlls.OnItemClick;
import com.thenextlogics.myapplication.controlls.DataBaseController;
import com.thenextlogics.myapplication.model.DuaModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DuaFragment extends Fragment implements View.OnClickListener {

    public DuaFragment() {
        // Required empty public constructor
    }
    RelativeLayout rl_back;
    TextView tv_toolbar_title;
    RecyclerView dua_recycler;
    LinearLayoutManager linearLayoutManager;
    List<DuaModel>duaModelList;
    ArrayList<DuaModel>duaModelArrayList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View roorView =  inflater.inflate(R.layout.fragment_dua, container, false);
        initView(roorView);
        return roorView;
    }
    private void initView(View view){
        tv_toolbar_title = (TextView)view.findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getText(R.string.dua));
        tv_toolbar_title.setTextColor(Color.BLACK);
        duaModelArrayList = new ArrayList<>();
        rl_back = (RelativeLayout) view.findViewById(R.id.rl_back);
        rl_back.setOnClickListener(this);
        dua_recycler = (RecyclerView)view.findViewById(R.id.dua_recycler);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        dua_recycler.setLayoutManager(linearLayoutManager);
        try {
            duaModelList= DataBaseController.getSingoltonInstance().getDuaTable(getActivity());
            for (int i = 0; i<duaModelList.size();i++){
                duaModelArrayList.add(duaModelList.get(i));
            }
            DuaAdaptar adaptar = new DuaAdaptar(duaModelList, new OnItemClick() {
                @Override
                public void onClick(int position) {
                    FragmentTrans.addFragmentwithRecycler(new DuaDetailFragment(),getActivity(),"id",position
                    ,"duaList",duaModelArrayList,DuaDetailFragment.class.getName());
                }
            });
            dua_recycler.setAdapter(adaptar);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_back:
                getFragmentManager().popBackStack();
                break;
            default:
                break;
        }
    }
}
