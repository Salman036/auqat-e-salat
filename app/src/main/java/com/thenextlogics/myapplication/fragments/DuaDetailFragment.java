package com.thenextlogics.myapplication.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.model.DuaModel;

import java.util.ArrayList;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class DuaDetailFragment extends Fragment implements View.OnClickListener {


    public DuaDetailFragment() {
        // Required empty public constructor
    }
   private TextView tv_dua_detail_ref,tv_dua_detail_arabic,tv_dua_detail_roman,tv_dua_detail_eng,toolbar_title;
 private    ArrayList<DuaModel>duaModelArrayList;
  private   int posotion;
    private RelativeLayout rl_save_back;
    private Button btn_save;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_dua_detail, container, false);
        initView(rootView);
        return rootView;
    }
    private void  initView(View view){
        tv_dua_detail_ref = (TextView)view.findViewById(R.id.tv_dua_detail_ref);
        btn_save = (Button) view.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        btn_save.setText("Share");
        tv_dua_detail_arabic = (TextView)view.findViewById(R.id.tv_dua_detail_arabic);
        tv_dua_detail_roman = (TextView)view.findViewById(R.id.tv_dua_detail_roman);
        tv_dua_detail_eng = (TextView)view.findViewById(R.id.tv_dua_detail_eng);
        toolbar_title = (TextView)view.findViewById(R.id.toolbar_title);
        rl_save_back = (RelativeLayout) view.findViewById(R.id.rl_save_back);
        toolbar_title.setTextColor(Color.BLACK);
        toolbar_title.setText("Dua Detail");
        tv_dua_detail_eng.setOnClickListener(this);

        rl_save_back.setOnClickListener(this);

        setValue();
    }
   private void shareText(){
       String shareString = tv_dua_detail_eng.getText().toString();
       Intent intent = new Intent(Intent.ACTION_SEND);
       intent.setType("text/plain");
       intent.putExtra(Intent.EXTRA_SUBJECT,"Subject Here");
       intent.putExtra(Intent.EXTRA_TEXT,shareString);
       startActivity(Intent.createChooser(intent,getResources().getString(R.string.share_using)));
   }
    private void setValue(){
        posotion = getArguments().getInt("id");
        duaModelArrayList = getArguments().getParcelableArrayList("duaList");
        if (duaModelArrayList!=null){
            if (duaModelArrayList.size()>0){
                tv_dua_detail_ref.setText(duaModelArrayList.get(posotion).getReference());
                tv_dua_detail_arabic.setText(duaModelArrayList.get(posotion).getArabic());
                tv_dua_detail_roman.setText(duaModelArrayList.get(posotion).getRoman());
                tv_dua_detail_eng.setText(duaModelArrayList.get(posotion).getEnglish());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_save_back:
                getFragmentManager().popBackStack();
                break;
            case R.id.btn_save:
                   shareText();
                break;

            default:
                break;
        }
    }

}
