package com.thenextlogics.myapplication.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.adaptar.AsmaUlNabiAdaptar;
import com.thenextlogics.myapplication.common.CustomeControlls.FragmentTrans;
import com.thenextlogics.myapplication.common.CustomeControlls.OnItemClick;
import com.thenextlogics.myapplication.controlls.DataBaseController;
import com.thenextlogics.myapplication.model.AsmaUlNabiModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AsmaUlNabiFragment extends Fragment implements View.OnClickListener {

//    List<AsmaUlNabiModel>asmaUlNabiModelList  = new ArrayList<>();
//    String title[] = {"Adil","Alim","Aalim","Abu al Qasim","Abu at Tahir","Abu at Tayab","Abu Ibrahim","Afoow","Aheed"};
//    String urdu[] = {"Adil","Alim","Aalim","Abu al Qasim","Abu at Tahir","Abu at Tayab","Abu Ibrahim","Afoow","Aheed"};
//    String meaning[] = {"The Justice","The Scholar","Slave of Allah","The Father Of Qasim","The Father of Thahir","The Father of Tayyab",
//                        "The Father of Ibtahim","Forgive","He was take to one side"};
    LinearLayoutManager linearLayoutManager;
    TextView tv_toolbar_title;
    RelativeLayout rl_back;


    List<AsmaUlNabiModel> asmaUlNabiModelList;
    ArrayList<AsmaUlNabiModel>asmaUlNabiModelArrayList;
        public AsmaUlNabiFragment() {
        // Required empty public constructor
    }
    RecyclerView asma_ulnabi_recycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_asma_ul_nabi, container, false);
        initView(rootView);
        return rootView;
    }
    private void initView(View view){
        tv_toolbar_title = (TextView)view.findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getText(R.string.asma_ul_nabi));

        rl_back = (RelativeLayout)view.findViewById(R.id.rl_back);
        rl_back.setOnClickListener(this);
        asmaUlNabiModelArrayList = new ArrayList<>();
        asma_ulnabi_recycler = (RecyclerView)view.findViewById(R.id.asma_ulnabi_recycler);
        linearLayoutManager = new LinearLayoutManager(getActivity());

        asma_ulnabi_recycler.setLayoutManager(linearLayoutManager);
        asma_ulnabi_recycler.setItemAnimator(new DefaultItemAnimator());
        try {
            asmaUlNabiModelList = DataBaseController.getSingoltonInstance().getAsmaUlNabiTable(getActivity());
            for (int i = 0;i<asmaUlNabiModelList.size();i++){
                asmaUlNabiModelArrayList.add(asmaUlNabiModelList.get(i));
            }
            AsmaUlNabiAdaptar adaptar = new AsmaUlNabiAdaptar(asmaUlNabiModelList, new OnItemClick() {
                @Override
                public void onClick(int position) {
                    FragmentTrans.addFragmentwithRecycler(new AsmaNabiDetailFragment(),getActivity(),"id",position
                    ,"nabiList",asmaUlNabiModelArrayList,AsmaNabiDetailFragment.class.getName());
                }
            });
            asma_ulnabi_recycler.setAdapter(adaptar);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_back:
                getFragmentManager().popBackStack();
                break;
            default:
                break;
        }
    }
}
