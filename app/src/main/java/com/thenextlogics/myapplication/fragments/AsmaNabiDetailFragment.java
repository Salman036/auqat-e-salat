package com.thenextlogics.myapplication.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.j256.ormlite.stmt.query.In;
import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.model.AsmaUlNabiModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class AsmaNabiDetailFragment extends Fragment implements View.OnClickListener {

    ArrayList<AsmaUlNabiModel> asmaUlNabiModelArrayList;
    public AsmaNabiDetailFragment() {
        // Required empty public constructor
    }
   private TextView toolbar_title;
    int position;
    RelativeLayout rl_save_back;
    Button btn_save;
    TextView tv_nabi_detail_mean,tv_nabi_detail_arabic_,tv_nabi_detail_eng;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_asma_nabi_detail, container, false);
        initView(rootView);
        return rootView;
    }
    private void initView(View view){
        rl_save_back = (RelativeLayout)view.findViewById(R.id.rl_save_back);
        rl_save_back.setOnClickListener(this);
        toolbar_title = (TextView) view.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Asma Ul Nabi Detail");
        toolbar_title.setTextColor(Color.BLACK);
        btn_save = (Button)view.findViewById(R.id.btn_save);
        btn_save.setText("Share");
       btn_save.setOnClickListener(this);
        tv_nabi_detail_eng = (TextView)view.findViewById(R.id.tv_nabi_detail_eng);
        tv_nabi_detail_arabic_ = (TextView)view.findViewById(R.id.tv_nabi_detail_arabic_);
        tv_nabi_detail_mean = (TextView)view.findViewById(R.id.tv_nabi_detail_mean);

        position = getArguments().getInt("id");
       asmaUlNabiModelArrayList = getArguments().getParcelableArrayList("nabiList");
        if (asmaUlNabiModelArrayList!=null){
            if (asmaUlNabiModelArrayList.size()>0){
                tv_nabi_detail_eng.setText(asmaUlNabiModelArrayList.get(position).getEnglish());
                tv_nabi_detail_arabic_.setText(asmaUlNabiModelArrayList.get(position).getArabic());
                tv_nabi_detail_mean.setText(asmaUlNabiModelArrayList.get(position).getMeaning());
            }

        }

    }
    private void shareText(){
        String shareString = tv_nabi_detail_eng.getText().toString();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT,"Subject Here");
        intent.putExtra(Intent.EXTRA_TEXT,shareString);
        startActivity(Intent.createChooser(intent,getResources().getString(R.string.share_using)));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_save_back:
                getFragmentManager().popBackStack();
                break;
            case R.id.btn_save:
              shareText();
                break;
            default:
                break;
        }
    }
}
