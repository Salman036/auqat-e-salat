package com.thenextlogics.myapplication.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thenextlogics.myapplication.R;

import com.thenextlogics.myapplication.controlls.DataBaseController;
import com.thenextlogics.myapplication.model.AsmaUlHusnaModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.support.v7.mediarouter.R.id.text;

/**
 * A simple {@link Fragment} subclass.
 */
public class AsmaUlHusnaDetailFragment extends Fragment implements View.OnClickListener {

    TextView tv_husna_detai_eng,tv_husna_detail_trans,tv_husna_detail_mening,tv_husna_detail_arabic,tv_husna_detail_vers_eng;
   private ArrayList<AsmaUlHusnaModel>asmaUlHusnaModelArrayList;

    String string;
    public AsmaUlHusnaDetailFragment() {
        // Required empty public constructor
    }
    int id;
    TextView tv_toolbar_title;
    RecyclerView husna_detail_recycler;
    RelativeLayout rl_save_back;
    Button btn_start;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_asma_ul_husna_detail, container, false);
        initView(rootView);
        return rootView;
    }
    private void initView(View view){
        btn_start = (Button)view.findViewById(R.id.btn_save);
        btn_start.setOnClickListener(this);
        btn_start.setText("Share");
        btn_start.setTextSize(14);
        tv_toolbar_title = (TextView)view.findViewById(R.id.toolbar_title);
        tv_toolbar_title.setText("Asma Ul Husna Detail");
        tv_toolbar_title.setTextColor(Color.BLACK);
        rl_save_back = (RelativeLayout)view.findViewById(R.id.rl_save_back);
        rl_save_back.setOnClickListener(this);
        tv_husna_detai_eng = (TextView)view.findViewById(R.id.tv_husna_detai_eng);
        tv_husna_detail_trans = (TextView)view.findViewById(R.id.tv_husna_detail_trans);
        tv_husna_detail_mening = (TextView)view.findViewById(R.id.tv_husna_detail_mening);
        tv_husna_detail_arabic = (TextView)view.findViewById(R.id.tv_husna_detail_arabic);
        tv_husna_detail_vers_eng = (TextView)view.findViewById(R.id.tv_husna_detail_vers_eng);
        tv_husna_detail_vers_eng.setOnClickListener(this);


        setValues();


    }

    public void setValues(){

        id=getArguments().getInt("id");

        asmaUlHusnaModelArrayList=getArguments().getParcelableArrayList("arrayList");


        if (asmaUlHusnaModelArrayList!=null){
            if (asmaUlHusnaModelArrayList.size()>0) {
                tv_husna_detai_eng.setText(asmaUlHusnaModelArrayList.get(id).getEnglish());
                tv_husna_detail_trans.setText(asmaUlHusnaModelArrayList.get(id).getTrans());
                tv_husna_detail_mening.setText(asmaUlHusnaModelArrayList.get(id).getMeaning());
                tv_husna_detail_arabic.setText(asmaUlHusnaModelArrayList.get(id).getVersus_arabic());
                tv_husna_detail_vers_eng.setText(asmaUlHusnaModelArrayList.get(id).getVersus_english());
            }
        }
    }
    private void shareText(){
        String shareString  = tv_husna_detail_vers_eng.getText().toString();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT,"Subject Here");
        intent.putExtra(Intent.EXTRA_TEXT,shareString);
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.share_using)));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_save_back:

                getFragmentManager().popBackStack();
                break;
            case R.id.btn_save:
                shareText();
                break;
            case R.id.tv_husna_detail_vers_eng:


                break;
            default:
                break;
        }
    }


}
