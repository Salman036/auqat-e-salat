package com.thenextlogics.myapplication.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.PrayTimeSchedule;
import com.thenextlogics.myapplication.model.PrayTimeModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrayHomeFragment extends Fragment {

    public PrayHomeFragment() {
        // Required empty public constructor
    }
    private TextView tv_pray_home_time;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_pray_home, container, false);
        initView(rootView);
        return  rootView;
    }
    private void initView(View view) {
//        pray_recycler = (RecyclerView)view.findViewById(R.id.pray_recycler);
//        linearLayoutManager = new LinearLayoutManager(getActivity());
//        pray_recycler.setLayoutManager(linearLayoutManager);
        tv_pray_home_time = (TextView) view.findViewById(R.id.tv_pray_home_time);
        // Retrive lat, lng using location API
        double latitude = 31.5546;
        double longitude = 74.3572;
        double timezone = (Calendar.getInstance().getTimeZone()
                .getOffset(Calendar.getInstance().getTimeInMillis()))
                / (1000 * 60 * 60);
        PrayTimeSchedule prayers = new PrayTimeSchedule();

        prayers.setTimeFormat(prayers.TIME_12);
        prayers.setCalcMethod(prayers.KARACHI);
        prayers.setAsrJuristic(prayers.SHAFII);
        prayers.setAdjustHighLats(prayers.ANGLE_BASED);
        int[] offsets = {0,0,0,0,0,0}; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
        prayers.tune(offsets);

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);

        ArrayList prayerTimes = prayers.getPrayerTimes(cal, latitude,
                longitude, timezone);
        ArrayList prayerNames = prayers.getTimeNames();

        for (int i = 0; i < prayerTimes.size(); i++) {
            tv_pray_home_time.append("\n" + prayerNames.get(i) + " -->" + prayerTimes.get(i));
        }
    }
}
