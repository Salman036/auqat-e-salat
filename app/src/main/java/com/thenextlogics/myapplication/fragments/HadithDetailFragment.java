package com.thenextlogics.myapplication.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.model.HadithModel;

import java.util.ArrayList;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class HadithDetailFragment extends Fragment implements View.OnClickListener {

    ArrayList<HadithModel>hadithModelArrayList;
    Button btn_stop;
    public HadithDetailFragment() {
        // Required empty public constructor
    }
    int position;
   private TextView hadith_detail_arabic,hadith_detail_english,tv_toolbar_title;
    private RelativeLayout rl_back;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_hadith_detail, container, false);
        initVIew(rootView);
        return rootView;
    }
    public void initVIew(View view){
        hadith_detail_arabic = (TextView)view.findViewById(R.id.hadith_detail_arabic);
        hadith_detail_english = (TextView)view.findViewById(R.id.hadith_detail_english);
        btn_stop = (Button) view.findViewById(R.id.btn_save);
        btn_stop.setOnClickListener(this);
        tv_toolbar_title = (TextView)view.findViewById(R.id.toolbar_title);
        tv_toolbar_title.setText("Hadith Detail");
        tv_toolbar_title.setTextColor(Color.BLACK);
        rl_back = (RelativeLayout)view.findViewById(R.id.rl_save_back);
        rl_back.setOnClickListener(this);

        setValues();

    }

    private void setValues(){
        position=getArguments().getInt("id");
        hadithModelArrayList = getArguments().getParcelableArrayList("hadithList");
        if (hadithModelArrayList!=null){
            if (hadithModelArrayList.size()>0) {
               hadith_detail_arabic.setText(hadithModelArrayList.get(position).getArabic_hadith());
                hadith_detail_english.setText(hadithModelArrayList.get(position).getHadees());
            }
        }
    }
    private void shareText(){
        String shareString = hadith_detail_english.getText().toString();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT,"Subject here");
        intent.putExtra(Intent.EXTRA_TEXT,shareString);
        startActivity(Intent.createChooser(intent,getResources().getString(R.string.share_using)));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_save_back:
                getFragmentManager().popBackStack();
                break;
            case R.id.btn_save:
                shareText();
                break;
            default:
                break;
        }
    }
}
