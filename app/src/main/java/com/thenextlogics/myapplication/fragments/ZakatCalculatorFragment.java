package com.thenextlogics.myapplication.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.common.CustomeControlls.FragmentTrans;

/**
 * A simple {@link Fragment} subclass.
 */
public class ZakatCalculatorFragment extends Fragment implements View.OnClickListener {


    public ZakatCalculatorFragment() {
        // Required empty public constructor
    }

    EditText et_num_one, et_num_two, et_result,et_pay_zakat;
    Button btn_result;
    double num1, num2,num3;
    double result = 0;
    double x= 2.5, y = 100;

    double multyplyAmount = 0;
    double zakatAmount = 0;
    private TextView tv_toolbar_title;
     RelativeLayout rl_back;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_zakat_calculator, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View view) {
        et_num_one = (EditText) view.findViewById(R.id.et_num_one);
        et_num_two = (EditText) view.findViewById(R.id.et_num_two);
        et_pay_zakat = (EditText) view.findViewById(R.id.et_pay_zakat);
        et_result = (EditText) view.findViewById(R.id.et_result);
        tv_toolbar_title = (TextView) view.findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getText(R.string.zakath_calculator));
        btn_result = (Button) view.findViewById(R.id.btn_result);
        rl_back = (RelativeLayout) view.findViewById(R.id.rl_back);


        btn_result.setOnClickListener(this);
        rl_back.setOnClickListener(this);
    }
    public void results(){
       if (et_num_one.getText().toString().isEmpty()){
           Toast.makeText(getActivity(),"Fill Weight",Toast.LENGTH_SHORT).show();
           et_num_one.setFocusable(true);
           return;
       }
        if (et_num_two.getText().toString().isEmpty()){
            Toast.makeText(getActivity(),"Please Fill Price",Toast.LENGTH_SHORT).show();
            et_num_two.setFocusable(true);
            return;
        }
        else {
            num1 = Double.parseDouble(et_num_one.getText().toString());
            num2 = Double.parseDouble(et_num_two.getText().toString());
            if (num1<=84.9){
                Toast.makeText(getActivity(),R.string.validate_zadat,Toast.LENGTH_LONG).show();
            }
            else {
                result = num1 * num2;
                multyplyAmount = result*x;
                zakatAmount = multyplyAmount/y;
                et_result.setText(String.valueOf(result));
                et_result.setTextColor(Color.RED);
                et_pay_zakat.setText(String.valueOf(zakatAmount));
                et_pay_zakat.setTextColor(Color.GREEN);
                et_pay_zakat.setTextSize(16);
            }

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_result:
//                FragmentTrans.addFragmentWithStack(new GoldFragment(),getActivity());
                results();
                et_num_one.setText("");

                et_num_two.setText("");
                break;
            case R.id.rl_back:
                getFragmentManager().popBackStack();
                break;
            default:
                break;
        }

    }
}
