package com.thenextlogics.myapplication.fragments;


import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.adaptar.AsmaUlHusnaAdaptar;
import com.thenextlogics.myapplication.common.CustomeControlls.FragmentTrans;
import com.thenextlogics.myapplication.common.CustomeControlls.OnItemClick;
import com.thenextlogics.myapplication.controlls.DataBaseController;
import com.thenextlogics.myapplication.model.AsmaUlHusnaModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class AsmaUlHusnaFragment extends Fragment implements View.OnClickListener {


    List<AsmaUlHusnaModel>asmaUlHusnaModelList;
    ArrayList<AsmaUlHusnaModel>asmaUlHusnaModelArrayList;
    ListView listView;
    public AsmaUlHusnaFragment() {
        // Required empty public constructor
    }
    RecyclerView husna_recycler;

    LinearLayoutManager linearLayoutManager;
    TextView tv_toolbar_title;
    RelativeLayout rl_back;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_asma_ul_husna, container, false);
        initView(rootView);
        return rootView;
    }
    private void initView(View view){
        husna_recycler = (RecyclerView)view.findViewById(R.id.husna_recycler);


        tv_toolbar_title = (TextView)view.findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getText(R.string.asma_ul_husna));
        asmaUlHusnaModelArrayList = new ArrayList<>();
        rl_back = (RelativeLayout)view.findViewById(R.id.rl_back);
        rl_back.setOnClickListener(this);

        linearLayoutManager = new LinearLayoutManager(getContext());
        husna_recycler.setLayoutManager(linearLayoutManager);


        try {
            asmaUlHusnaModelList = DataBaseController.getSingoltonInstance().getAsmaUlHusnaTable(getActivity());
            for (int i=0; i<asmaUlHusnaModelList.size();i++){
                asmaUlHusnaModelArrayList.add(asmaUlHusnaModelList.get(i));
            }
            AsmaUlHusnaAdaptar adaptar = new AsmaUlHusnaAdaptar(asmaUlHusnaModelList, new OnItemClick() {
                @Override
                public void onClick(int position) {
//                    Intent intent = new Intent(getActivity(),AsmaUlHusnaDetailFragment.class);
//                    intent.putExtra("arrayList",asmaUlHusnaModelList.get(position).getId());
//                    startActivity(intent);
                    FragmentTrans.addFragmentwithRecycler(new AsmaUlHusnaDetailFragment(),getActivity(),"id",position,"arrayList"
                            ,asmaUlHusnaModelArrayList,AsmaUlHusnaDetailFragment.class.getName());
                }
            });
            husna_recycler.setAdapter(adaptar);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_back:
                getFragmentManager().popBackStack();
                break;
            default:
                break;
        }
    }
}
