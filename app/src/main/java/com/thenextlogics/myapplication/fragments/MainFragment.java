package com.thenextlogics.myapplication.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thenextlogics.myapplication.R;
import com.thenextlogics.myapplication.adaptar.TabAdaptar;

import me.relex.circleindicator.CircleIndicator;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener {


    public MainFragment() {
        // Required empty public constructor
    }
    TabAdaptar tabAdaptar;
    ViewPager viewPager;
    CircleIndicator indicator;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_main, container, false);
        initView(rootView);
        return rootView;
    }
    public void initView(View view){
        viewPager = (ViewPager)view.findViewById(R.id.view_pager);
        tabAdaptar = new TabAdaptar(getChildFragmentManager());
        viewPager.setAdapter(tabAdaptar);
        viewPager.setCurrentItem(1);
        indicator = (CircleIndicator)view.findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }
}
