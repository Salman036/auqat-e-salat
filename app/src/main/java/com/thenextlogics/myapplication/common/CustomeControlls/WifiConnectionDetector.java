package com.thenextlogics.myapplication.common.CustomeControlls;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Salman Saleem on 1/29/2017.
 */

public class WifiConnectionDetector {
    Context context;
    public WifiConnectionDetector(Context context){
        this.context = context;
    }
    public boolean checkmobileConnection() {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            //Get network info - WIFI internet access
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            if (info != null) {
                //Look for whether device is currently connected to WIFI network
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;

    }
}
