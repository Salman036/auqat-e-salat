package com.thenextlogics.myapplication.common.CustomeControlls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.thenextlogics.myapplication.controlls.AppControler;


/**
 * Created by Salman Saleem on 1/16/2017.
 */

public class OtfTextView extends TextView {
    public OtfTextView(Context context) {
        super(context);
        init();
    }

    public OtfTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public OtfTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

   private void init(){
       Typeface typeface = AppControler.getSingletonInstance().typefaceOtf;
       setTypeface(typeface);
   }
}
