package com.thenextlogics.myapplication.common.CustomeControlls;

import android.content.Context;
import android.content.SharedPreferences;

import com.thenextlogics.myapplication.fragments.DailyAdhkarFragment;

/**
 * Created by Salman Saleem on 1/25/2017.
 */

public class PreferencesManager {
    public static void saveValues(Context context, String key, String value){
        SharedPreferences pref = context.getSharedPreferences(DailyAdhkarFragment.MyPrefrence, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static String getPreferences(Context context,String key){
        SharedPreferences pref = context.getSharedPreferences(DailyAdhkarFragment.MyPrefrence, context.MODE_PRIVATE);

        return  pref.getString(key, null);
    }
    public static void clear(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();

    }
    public static void removeValue(Context context,String PREF_KEY) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(DailyAdhkarFragment.MyPrefrence, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.remove(PREF_KEY);
        editor.commit();
    }
}
