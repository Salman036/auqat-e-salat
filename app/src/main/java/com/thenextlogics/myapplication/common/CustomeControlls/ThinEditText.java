package com.thenextlogics.myapplication.common.CustomeControlls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.thenextlogics.myapplication.controlls.AppControler;


/**
 * Created by Salman Saleem on 1/16/2017.
 */

public class ThinEditText extends EditText {
    public ThinEditText(Context context) {
        super(context);
    }

    public ThinEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ThinEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public void init(){
        Typeface typeface = AppControler.getSingletonInstance().typefaceThin;
        setTypeface(typeface);
    }
}
