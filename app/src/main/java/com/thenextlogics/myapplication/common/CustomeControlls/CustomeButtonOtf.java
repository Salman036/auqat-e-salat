package com.thenextlogics.myapplication.common.CustomeControlls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.thenextlogics.myapplication.controlls.AppControler;


/**
 * Created by Salman Saleem on 1/16/2017.
 */

public class CustomeButtonOtf extends Button {
    public CustomeButtonOtf(Context context) {
        super(context);
        init();
    }

    public CustomeButtonOtf(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomeButtonOtf(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public void init(){
        Typeface typeface = AppControler.getSingletonInstance().typefaceOtf;
        setTypeface(typeface);
    }
}
