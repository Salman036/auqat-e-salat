package com.thenextlogics.myapplication.common.CustomeControlls;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.thenextlogics.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Salman Saleem on 1/18/2017.
 */

public class FragmentTrans {

    public static void addFragmentWithStack(Fragment fragment, FragmentActivity fragmentActivity){
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fram_content,fragment);
        ft.addToBackStack(fragment.getClass().getName());
        ft.commit();

    }
    public static void addFragmentwithOutStackandBundle(Fragment f, FragmentActivity activity) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fram_content, f);
        Bundle bundle=new Bundle();
        f.setArguments(bundle);
        ft.commit();
    }
    public static void addFragmentwithRecycler(Fragment f, FragmentActivity activity, String key1, int value2, String key, ArrayList value, String backStackKey) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fram_content, f);
        ft.addToBackStack(backStackKey);
        Bundle args = new Bundle();
        args.putInt(key1,value2);
        args.putParcelableArrayList(key, value);
        f.setArguments(args);
        ft.commit();
    }
    public static void replaceFragmentwithOutStack(Fragment f, FragmentActivity activity) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fram_content, f);
        ft.addToBackStack(null);
        ft.commit();
    }
    public static void addFragment(Fragment f, FragmentActivity activity) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fram_content, f);
        ft.commit();
    }
    public static void addFragmentAnimation(Fragment fragment , FragmentActivity fragmentActivity){
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.fram_content,fragment);
        ft.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_in_left);
        ft.addToBackStack(null);
        ft.commit();

    }
}
