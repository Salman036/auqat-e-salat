package com.thenextlogics.myapplication.common.CustomeControlls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


import com.thenextlogics.myapplication.controlls.AppControler;

/**
 * Created by Salman Saleem on 1/16/2017.
 */

public class ThinTextView extends TextView {
    private Typeface typeface;
    public ThinTextView(Context context) {
        super(context);
        init();
    }

    public ThinTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ThinTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init(){
        typeface = AppControler.getSingletonInstance().typefaceThin;
        setTypeface(typeface);
    }
}
