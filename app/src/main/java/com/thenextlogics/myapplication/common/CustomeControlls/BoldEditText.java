package com.thenextlogics.myapplication.common.CustomeControlls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;


import com.thenextlogics.myapplication.controlls.AppControler;

/**
 * Created by Salman Saleem on 1/16/2017.
 */

public class BoldEditText extends EditText {
    public BoldEditText(Context context) {
        super(context);
        init();
    }

    public BoldEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoldEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public void init(){
        Typeface typeface = AppControler.getSingletonInstance().typefaceBold;
        setTypeface(typeface);
    }
}
