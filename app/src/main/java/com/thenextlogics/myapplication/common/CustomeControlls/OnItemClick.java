package com.thenextlogics.myapplication.common.CustomeControlls;

/**
 * Created by Salman Saleem on 1/20/2017.
 */

public interface OnItemClick {
    public  void onClick(int position);
}
