package com.thenextlogics.myapplication.common.CustomeControlls;

import android.content.Context;



/**
 * Created by Mobeen Altaf on 12/16/2015.
 */
public class HijriUtils {

//    public static double LATITUDE_MAKKAH = 21.4166667000000;
//    public static double LONGITUDE_MAKKAH = 39.81666670000004;

    public static double LATITUDE_MAKKAH = 21.422528;
    public static double LONGITUDE_MAKKAH = 39.826369;


    private static double pi = 3.141592653589793;

//    public static String getHijriMonthArabicName(Context mContext, int hijMonth) {
//        String hij_month_name = "";
//        if (hijMonth == 1) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_one);
//        } else if (hijMonth == 2) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_two);
//        } else if (hijMonth == 3) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_three);
//        } else if (hijMonth == 4) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_four);
//        } else if (hijMonth == 5) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_five);
//        } else if (hijMonth == 6) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_six);
//        } else if (hijMonth == 7) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_seven);
//        } else if (hijMonth == 8) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_eight);
//        } else if (hijMonth == 9) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_nine);
//        } else if (hijMonth == 10) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_ten);
//        } else if (hijMonth == 11) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_eleven);
//        } else if (hijMonth == 12) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_twelve);
//        }
//        return hij_month_name;
//    }
//    public static String getHijriMonthArabicNameCalendar(Context mContext, int hijMonth) {
//        String hij_month_name = "";
//        if (hijMonth == 1) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_one);
//        } else if (hijMonth == 2) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_two);
//        } else if (hijMonth == 3) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_three_1);
//        } else if (hijMonth == 4) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_four_2);
//        } else if (hijMonth == 5) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_five);
//        } else if (hijMonth == 6) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_six);
//        } else if (hijMonth == 7) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_seven);
//        } else if (hijMonth == 8) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_eight);
//        } else if (hijMonth == 9) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_nine);
//        } else if (hijMonth == 10) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_ten);
//        } else if (hijMonth == 11) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_eleven);
//        } else if (hijMonth == 12) {
//            hij_month_name = mContext.getResources().getString(R.string.hijri_eng_twelve);
//        }
//        return hij_month_name;
//    }

    public static float getQibleFromNorth(double latitude, double langitude) {
        double fLat = (latitude * pi / 180.0f);
        double fLng = (langitude * pi / 180.0f);
        double tLat = (LATITUDE_MAKKAH * pi / 180.0f);
        double tLng = (LONGITUDE_MAKKAH * pi / 180.0f);

        double degre = (Math.atan2(Math.sin(tLng - fLng) * Math.cos(tLat), Math.cos(fLat) * Math.sin(tLat) - Math.sin(fLat) * Math.cos(tLat) * Math.cos(tLng - fLng)) * 180.0f / pi);

        if (degre <= 0) {
            degre = 360 + degre;
        }
        return (float) degre;
    }

//    public static String getPrayerCalcMethodName(Context mContext,int method){
//        int name = -1;
//        switch (method){
//            case 1:
//                name = R.string.univ_karachi;
//                break;
//            case 2:
//                name = R.string.btn_islamic_america;
//                break;
//            case 3:
//                name = R.string.btn_muslim_wleague;
//                break;
//            case 4:
//                name = R.string.uml_qura;
//                break;
//            case 5:
//                name = R.string.egypt_auth;
//                break;
//            case 6:
//                name = R.string.tehran_uni;
//                break;
//            case 7:
//                name = R.string.custom_angle;
//                break;
//            case 8:
//                name = R.string.fix_isha;
//                break;
//            case 9:
//                name = R.string.france;
//                break;
//            case 10:
//                name = R.string.london;
//                break;
//            case 11:
//                name = R.string.gulf;
//                break;
//        }
//        if(name == -1)return "";
//        return mContext.getString(name);
//    }
}
