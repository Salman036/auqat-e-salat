package com.thenextlogics.myapplication.controlls;

import android.app.Application;
import android.graphics.Typeface;

import com.thenextlogics.myapplication.common.CustomeControlls.Constant;
import com.thenextlogics.myapplication.common.CustomeControlls.Utils;

import java.io.File;
import java.util.Date;

/**
 * Created by Salman Saleem on 1/18/2017.
 */

public class AppControler extends Application {
    public Typeface typefaceOtf;
    public Typeface typefaceBold;
    public Typeface typefaceThin;
    Date dateNow;

    public static AppControler appControler;

    @Override
    public void onCreate() {
        super.onCreate();


       appControler = this;
        appControler = this;
        typefaceOtf=Typeface.createFromAsset(getAssets(),"fonts/ABChanel2014Couture-ExLig.otf");
        typefaceThin = Typeface.createFromAsset(getAssets(),"fonts/CaviarDreams.ttf");
        typefaceBold = Typeface.createFromAsset(getAssets(),"fonts/Caviar_Dreams_Bold.ttf");

    }
    public static AppControler getSingletonInstance(){
        if(appControler ==null)   {
            appControler =new AppControler();


        }
        return appControler;
    }
}
