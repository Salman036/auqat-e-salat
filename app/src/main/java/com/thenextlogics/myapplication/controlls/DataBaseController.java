package com.thenextlogics.myapplication.controlls;

import android.content.Context;

import com.thenextlogics.myapplication.databse.DataBaseHelper;
import com.thenextlogics.myapplication.model.AsmaUlHusnaModel;
import com.thenextlogics.myapplication.model.AsmaUlNabiModel;
import com.thenextlogics.myapplication.model.DuaModel;
import com.thenextlogics.myapplication.model.HadithModel;
import com.thenextlogics.myapplication.model.QuranModel;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Salman Saleem on 1/19/2017.
 */

public class DataBaseController {

    private static DataBaseController dataBaseController;
    public DataBaseController(){

    }
    public static DataBaseController getSingoltonInstance(){
        if (dataBaseController == null){
            dataBaseController = new DataBaseController();
        }
        return dataBaseController;
    }
    public List<AsmaUlNabiModel>getAsmaUlNabiTable(Context context) throws SQLException{
        return DataBaseHelper.getSingletonInstance(context)
                .getAsmaUlNabiModelIntegerDao()
                .queryBuilder().query();
    }
    public List<AsmaUlHusnaModel>getAsmaUlHusnaTable(Context context)throws SQLException{
        return DataBaseHelper.getSingletonInstance(context).getAsmaUlHusnaModelIntegerDao()
                .queryBuilder().query();
    }
    public List<DuaModel>getDuaTable(Context context)throws SQLException{
         return DataBaseHelper.getSingletonInstance(context).getDuaModelIntegerDao()
                .queryBuilder().query();
    }
    public List<HadithModel>getHadithTable(Context context)throws SQLException{
        return DataBaseHelper.getSingletonInstance(context).getHadithModelIntegerDao()
                .queryBuilder().query();
    }
    public List<QuranModel>getQuran(Context context)throws SQLException{
        return DataBaseHelper.getSingletonInstance(context).getQuranModelIntegerDao()
                .queryBuilder().query();
    }
}
