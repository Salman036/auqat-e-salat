package com.thenextlogics.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Salman Saleem on 1/20/2017.
 */
@DatabaseTable (tableName = "duas")
public class DuaModel implements Parcelable {
    @DatabaseField (id = true)
    private int id;
    @DatabaseField
    private String arabic;
    @DatabaseField
    private String reference;
    @DatabaseField
    private String roman;
    @DatabaseField
    private String english;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArabic() {
        return arabic;
    }

    public void setArabic(String arabic) {
        this.arabic = arabic;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRoman() {
        return roman;
    }

    public void setRoman(String roman) {
        this.roman = roman;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }
    public DuaModel(){

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(arabic);
        dest.writeString(reference);
        dest.writeString(roman);
        dest.writeString(english);
    }
    protected DuaModel(Parcel in) {
        id = in.readInt();
        arabic = in.readString();
        reference = in.readString();
        roman = in.readString();
        english = in.readString();
    }

    public static final Creator<DuaModel> CREATOR = new Creator<DuaModel>() {
        @Override
        public DuaModel createFromParcel(Parcel in) {
            return new DuaModel(in);
        }

        @Override
        public DuaModel[] newArray(int size) {
            return new DuaModel[size];
        }
    };
}
