package com.thenextlogics.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Salman Saleem on 1/18/2017.
 */
@DatabaseTable (tableName = "asmaNabi")
public class AsmaUlNabiModel implements Parcelable {
    @DatabaseField(id = true)
    private int id;
    @DatabaseField
    private String arabic;
    @DatabaseField
    private String english;
    @DatabaseField
    private String meaning;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArabic() {
        return arabic;
    }

    public void setArabic(String arabic) {
        this.arabic = arabic;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.arabic);
        dest.writeString(this.english);
        dest.writeString(this.meaning);
    }
    public AsmaUlNabiModel() {
    }

    protected AsmaUlNabiModel(Parcel in) {
        this.id = in.readInt();
        this.arabic = in.readString();
        this.english = in.readString();
        this.meaning = in.readString();
    }

    public static final Parcelable.Creator<AsmaUlNabiModel> CREATOR = new Parcelable.Creator<AsmaUlNabiModel>() {
        @Override
        public AsmaUlNabiModel createFromParcel(Parcel source) {
            return new AsmaUlNabiModel(source);
        }

        @Override
        public AsmaUlNabiModel[] newArray(int size) {
            return new AsmaUlNabiModel[size];
        }
    };
}
