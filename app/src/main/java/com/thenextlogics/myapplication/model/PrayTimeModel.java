package com.thenextlogics.myapplication.model;


/**
 * Created by Salman Saleem on 1/18/2017.
 */

public class PrayTimeModel {
  private String prayTime;
    private String prayName;

    public String getPrayTime() {
        return prayTime;
    }

    public void setPrayTime(String prayTime) {
        this.prayTime = prayTime;
    }

    public String getPrayName() {
        return prayName;
    }

    public void setPrayName(String prayName) {
        this.prayName = prayName;
    }
}
