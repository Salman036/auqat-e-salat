package com.thenextlogics.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Salman Saleem on 1/18/2017.
 */
@DatabaseTable (tableName = "names")
public class AsmaUlHusnaModel  implements Parcelable{
    @DatabaseField(id = true)
    private int id;
    @DatabaseField
    private String arabic;
    @DatabaseField
    private String english;
    @DatabaseField
    private String trans;
    @DatabaseField
    private String meaning;
    @DatabaseField
    private String reference;
    @DatabaseField
    private String versus_english;
    @DatabaseField
    private String versus_arabic;

    public String getArabic() {
        return arabic;
    }

    public void setArabic(String arabic) {
        this.arabic = arabic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getVersus_english() {
        return versus_english;
    }

    public void setVersus_english(String versus_english) {
        this.versus_english = versus_english;
    }

    public String getVersus_arabic() {
        return versus_arabic;
    }

    public void setVersus_arabic(String versus_arabic) {
        this.versus_arabic = versus_arabic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.arabic);
        dest.writeString(this.english);
        dest.writeString(this.trans);
        dest.writeString(this.meaning);
        dest.writeString(this.reference);
        dest.writeString(this.versus_english);
        dest.writeString(this.versus_arabic);
    }

    public AsmaUlHusnaModel() {
    }

    protected AsmaUlHusnaModel(Parcel in) {
        this.id = in.readInt();
        this.arabic = in.readString();
        this.english = in.readString();
        this.trans = in.readString();
        this.meaning = in.readString();
        this.reference = in.readString();
        this.versus_english = in.readString();
        this.versus_arabic = in.readString();
    }

    public static final Parcelable.Creator<AsmaUlHusnaModel> CREATOR = new Parcelable.Creator<AsmaUlHusnaModel>() {
        @Override
        public AsmaUlHusnaModel createFromParcel(Parcel in) {
            return new AsmaUlHusnaModel(in);
        }

        @Override
        public AsmaUlHusnaModel[] newArray(int size) {
            return new AsmaUlHusnaModel[size];
        }
    };
}
