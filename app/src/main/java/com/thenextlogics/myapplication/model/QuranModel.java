package com.thenextlogics.myapplication.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Salman Saleem on 1/27/2017.
 */
@DatabaseTable (tableName = "quran-simple")
public class QuranModel {
    @DatabaseField (generatedId = true)
    private int rowid;
    @DatabaseField
    private String iquran;

    public int getRowid() {
        return rowid;
    }

    public void setRowid(int rowid) {
        this.rowid = rowid;
    }

    public String getIquran() {
        return iquran;
    }

    public void setIquran(String iquran) {
        this.iquran = iquran;
    }
}
