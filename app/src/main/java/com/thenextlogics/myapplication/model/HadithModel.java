package com.thenextlogics.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Salman Saleem on 1/23/2017.
 */
@DatabaseTable (tableName = "ahadees")
public class HadithModel implements Parcelable  {


    @DatabaseField (generatedId = true)
    private int id;
    @DatabaseField
    private String hadees;
    @DatabaseField
    private String arabic_hadith;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHadees() {
        return hadees;
    }

    public void setHadees(String hadees) {
        this.hadees = hadees;
    }

    public String getArabic_hadith() {
        return arabic_hadith;
    }

    public void setArabic_hadith(String arabic_hadith) {
        this.arabic_hadith = arabic_hadith;
    }
    protected HadithModel(Parcel in) {
        id = in.readInt();
        hadees = in.readString();
        arabic_hadith = in.readString();
    }
    public HadithModel(){

    }
    public static final Creator<HadithModel> CREATOR = new Creator<HadithModel>() {
        @Override
        public HadithModel createFromParcel(Parcel in) {
            return new HadithModel(in);
        }

        @Override
        public HadithModel[] newArray(int size) {
            return new HadithModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(hadees);
        dest.writeString(arabic_hadith);
    }
}
